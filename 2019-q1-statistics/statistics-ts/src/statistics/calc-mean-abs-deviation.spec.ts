import { calcMeanAbsoluteDeviation } from "./calc-mean-abs-deviation";

describe("calcMeanAbsoluteDeviation", () => {
  test("should return correct value", () => {
    expect(calcMeanAbsoluteDeviation([1, 2, 3])).toBe(1);
    expect(calcMeanAbsoluteDeviation([1, 1, 1])).toBe(0);
  });
});
