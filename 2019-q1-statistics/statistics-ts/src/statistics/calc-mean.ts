import { checkIsArrayEmpty, checkIsNumberSafe, sum } from "../utils";

export const calcMean = (values: number[]) => {
  const s = sum(values);
  return s / values.length;
};

export default calcMean;
