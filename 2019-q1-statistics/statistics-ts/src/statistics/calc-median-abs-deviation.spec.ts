import { calcMedianAbsoluteDeviation } from "./calc-median-abs-deviation";

describe("calcMedianAbsoluteDeviation", () => {
  test("should return correct value", () => {
    expect(calcMedianAbsoluteDeviation([1, 2, 3])).toBe(1);
    expect(calcMedianAbsoluteDeviation([1, 1, 1])).toBe(0);
  });
});
