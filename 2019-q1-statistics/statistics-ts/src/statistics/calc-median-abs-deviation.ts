import { calcMedian } from "./calc-median";

export const calcMedianAbsoluteDeviation = (values: number[]) => {
  const median = calcMedian(values);
  const sumOfAbsDeviation = values.reduce(
    (a, c) => a + Math.abs(c - median),
    0
  );
  return sumOfAbsDeviation / median;
};
