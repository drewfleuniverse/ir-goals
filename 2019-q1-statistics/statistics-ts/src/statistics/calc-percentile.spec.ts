import { calcPercentileNearest } from "./calc-percentile";

describe("calcPercentileNearest", () => {
  test("should return correct percentile", () => {
    const seq = [15, 20, 35, 40, 50];
    expect(calcPercentileNearest(seq, 5)).toBe(15);
    expect(calcPercentileNearest(seq, 30)).toBe(20);
    expect(calcPercentileNearest(seq, 40)).toBe(20);
    expect(calcPercentileNearest(seq, 50)).toBe(35);
    expect(calcPercentileNearest(seq, 100)).toBe(50);
  });
});
