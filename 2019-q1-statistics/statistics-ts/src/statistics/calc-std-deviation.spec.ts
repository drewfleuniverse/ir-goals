import { calcStandardDeviation } from "./calc-std-deviation";

describe("calcStandardDeviation", () => {
  test("should return correct value", () => {
    expect(calcStandardDeviation([1, 2, 3])).toBe(0.816496580927726);
  });
});
