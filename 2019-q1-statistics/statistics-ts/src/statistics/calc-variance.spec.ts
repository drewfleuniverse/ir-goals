import { calcVariance } from "./calc-variance";

describe("calcVariance", () => {
  test("should return correct calcVariance", () => {
    expect(calcVariance([1, 2, 3])).toBeCloseTo(0.67);
    expect(calcVariance([1, 1, 1])).toBe(0);
  });
});
