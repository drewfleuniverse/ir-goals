import { sum } from "../utils";
import { calcMean } from "./calc-mean";

export const calcVariance = (values: number[]) => {
  const m = calcMean(values);
  const diff = values.map(v => v - m);
  const squaredDiff = diff.map(d => Math.pow(d, 2));
  return sum(squaredDiff) / values.length;
};
