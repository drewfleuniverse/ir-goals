import { checkIsArrayEmpty } from "./check-is-array-empty";

describe("checkIsArrayEmpty", () => {
  test("should not throw when array is not empty", () => {
    expect(() => checkIsArrayEmpty([42])).not.toThrow();
  });
  test("should throw when array is empty", () => {
    expect(() => checkIsArrayEmpty([])).toThrow();
  });
});
