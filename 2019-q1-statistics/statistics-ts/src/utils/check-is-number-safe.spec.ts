import { checkIsNumberSafe } from "./check-is-number-safe";

describe("checkIsNumberSafe", () => {
  const { MAX_SAFE_INTEGER: MAX, MIN_SAFE_INTEGER: MIN } = Number;
  describe("Check num", () => {
    test("should throw when value is unsafe", () => {
      expect(() => checkIsNumberSafe(MAX + 1)).toThrow();
      expect(() => checkIsNumberSafe(MIN - 1)).toThrow();
    });
    test("should not throw when lower and higher bounds are omitted", () => {
      expect(() => checkIsNumberSafe(0)).not.toThrow();
    });
  });
  describe("Check bounds", () => {
    test("should not throw when number is within bound", () => {
      expect(() => checkIsNumberSafe(0, -1, 1)).not.toThrow();
    });
    test("should throw when lower bound is larger than higher bound", () => {
      expect(() => checkIsNumberSafe(0, 1, -1)).toThrow();
    });
  });
  describe("Check lower bound", () => {
    test("should not throw when lower bound is safe", () => {
      expect(() => checkIsNumberSafe(0, MIN)).not.toThrow();
    });
    test("should throw when lower bound is unsafe", () => {
      expect(() => checkIsNumberSafe(0, MIN - 1)).toThrow();
    });
    test("should throw when lower bound is larger than num", () => {
      expect(() => checkIsNumberSafe(0, 1)).toThrow();
    });
  });
  describe("Check higher bound", () => {
    test("should not throw when higher bound is safe", () => {
      expect(() => checkIsNumberSafe(0, undefined, MAX)).not.toThrow();
    });
    test("should throw when higher bound is unsafe", () => {
      expect(() => checkIsNumberSafe(0, undefined, MAX + 1)).toThrow();
    });
    test("should throw when higher bound is smaller than num", () => {
      expect(() => checkIsNumberSafe(0, undefined, -1)).toThrow();
    });
  });
});
