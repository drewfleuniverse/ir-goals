import { isSafeNumber } from "./is-safe-number";

export const checkIsNumberSafe = (num: number, lo?: number, hi?: number) => {
  if (!isSafeNumber(num)) {
    throw new Error(`Number ${num} is unsafe.`);
  }
  if (
    lo !== undefined &&
    hi !== undefined &&
    isSafeNumber(lo) &&
    isSafeNumber(hi) &&
    lo > hi
  ) {
    throw new Error(`Lower bound ${lo} is larger than higher bound ${hi}.`);
  }
  if (lo !== undefined) {
    if (!isSafeNumber(lo)) {
      throw new Error(`Lower bound ${lo} is unsafe.`);
    }
    if (num < lo) {
      throw new Error(`Number ${num} is smaller than lower bound ${lo}.`);
    }
  }
  if (hi !== undefined) {
    if (!isSafeNumber(hi)) {
      throw new Error(`Higher bound ${hi} is unsafe.`);
    }
    if (num > hi) {
      throw new Error(`Number ${num} is larger than higher bound ${hi}.`);
    }
  }
};

export default checkIsNumberSafe;
