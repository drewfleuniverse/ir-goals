import { checkIsNumbersSafe } from "./check-is-numbers-safe";

describe("checkIsNumbersSafe", () => {
  test("should not throw when values contains safe number", () => {
    expect(() => checkIsNumbersSafe([42])).not.toThrow();
  });

  test("should throw when values contains unsafe number", () => {
    const { MAX_SAFE_INTEGER: MAX, MIN_SAFE_INTEGER: MIN } = Number;
    expect(() => checkIsNumbersSafe([MAX + 1])).toThrow();
    expect(() => checkIsNumbersSafe([MIN - 1])).toThrow();
  });
});
