import { getUnsafeNumberIndex } from "./get-unsafe-number-index";

describe("getUnsafeNumberIndex", () => {
  test("should return -1 when values contains safe number", () => {
    expect(getUnsafeNumberIndex([42])).toBe(-1);
  });

  test("should return the index of unsafe number", () => {
    const { MAX_SAFE_INTEGER: MAX, MIN_SAFE_INTEGER: MIN } = Number;
    expect(getUnsafeNumberIndex([42, MAX + 1])).toBe(1);
    expect(getUnsafeNumberIndex([MIN - 1, 42])).toBe(0);
  });
});
