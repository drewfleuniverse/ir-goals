import { isSafeNumber } from "./is-safe-number";

export const getUnsafeNumberIndex = (values: number[]) => {
  let index;
  const hasUnsafe = values.some((n, i) => {
    index = i;
    return !isSafeNumber(n);
  });
  return hasUnsafe ? index : -1;
};

export default getUnsafeNumberIndex;
