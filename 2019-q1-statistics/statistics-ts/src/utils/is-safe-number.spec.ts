import { isSafeNumber } from './is-safe-number';

describe('isSafeNumber', () => {
  const { MAX_SAFE_INTEGER: MAX, MIN_SAFE_INTEGER: MIN } = Number;

  test('should be truthy when input is smaller than and equal to the upper bound', () => {
    expect(isSafeNumber(MAX)).toBeTruthy();
  });
  test('should be truthy when input is larger than or equal to the upper bound', () => {
    expect(isSafeNumber(MIN)).toBeTruthy();
  });
  test('should be falsy when input is larger than the upper bound', () => {
    expect(isSafeNumber(MAX + 1)).toBeFalsy();
  });
  test('should be falsy when input is smaller than the lower bound', () => {
    expect(isSafeNumber(MIN - 1)).toBeFalsy();
  });
});
