import { sort } from "./sort";

describe("sort", () => {
  let values: number[];
  beforeEach(() => {
    values = [2, 3, 1];
  });
  test("should sort values by acending order by default", () => {
    expect(sort(values)).toMatchObject([1, 2, 3]);
  });
  test("should sort values by acending order", () => {
    expect(sort(values, "asc")).toMatchObject([1, 2, 3]);
  });
  test("should sort values by decending order", () => {
    expect(sort(values, "desc")).toMatchObject([3, 2, 1]);
  });
});
