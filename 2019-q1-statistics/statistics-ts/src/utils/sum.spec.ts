import { sum } from "./sum";

describe("sum", () => {
  test("should return correct sum", () => {
    expect(sum([1, 2, 3])).toBe(6);
  });
});
