import { checkIsArrayEmpty, checkIsNumberSafe } from "./index";

export const sum = (values: number[]) => {
  return values.reduce((acc, cur) => {
    const s = acc + cur;
    return s;
  });
};

export default sum;
