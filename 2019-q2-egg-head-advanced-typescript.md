# Practical Advanced TypeScript

This is my learning notes for [EggHead course](https://egghead.io/courses/practical-advanced-typescript).

## 1: Numeric seperator

```ts
const n = 1_234; // js: var n = 1234;
settimeout(/*...*/, 1_000); // js: settimeout(/*...*/, 1000);
```

## 2: Nullness checks

```ts
// Without nullness checks
const a: string[]; // Uninitialized array
a.filter(el => el); // No error at compile time but throws at runtime

// With nullness checks
// Add `strictPropertyInitialization: true` and `strictNullChecks: true`
const a: string[] = []; // Properly initialize array
a.filter(el => el); // No error at runtime

// Using non-null operator (!) to escape nullness check
// Contrived example
const a: sring[];
a!.filter(/*...*/);
```

## 3: Dynamic type inference with `in` operator

```ts
interface A {
  x: string;
}
interface B {
  y: string;
}
function f(p: A | B) {
  if (‘x’ in p) {
    p.x = 42; // p is type A
  } else {
    p.y = 42; // p is type B
  }
}
```

## 4: Type inference

```ts
// Type inference to a fixed value

const z = ‘r’;
z; // Has type ‘r’

interface IA {
  x: string;
  y: string;
}

class A implements IA {
  x = ‘p’;
  readonly y = ‘q’;
}
const a = new A()
a.x; // Has type string
a.y; // Has type ‘q’

interface IB extends IA {
  y: ‘q’;
}
const b: IB {
  x: ‘p’;
  y: ‘q’; // y can only have value ‘q’
}
b.x; // Has type string
b.y; // Has type ‘q’


// A redux example

interface Action {
  type: string;
}
class A implements Action {
  readonly type = ‘a’;
}
class B implements Action {
  readonly type = ‘b’;
  constructor(public payload: number) {}
}
const myReducer = (action: Action, state: string): string => {
  switch(action.type) {
    case ‘a’:
      return ‘just a’;
    case ‘b’:
      return `b and ${action.payload}`;
    default:
      const x: never = action;
      // ^: Compile time type check to make sure `action` is never fell to default
  }
};
```

## 5: Using type modifiers to improve type readability

```ts
interface IA {
  x?: string:
}
type A1 = {
  readonly [K in keyof IA]: IA[K]; // Optional props remain optional
};
type A2 = {
  readonly [K in keyof IA]-?: IA[K]; // Optional props are now required
};
type A3 = {
  readonly [K in keyof IA]?: IA[K]; // All props are optional
};
type A4 = {
  readonly [K in keyof IA]+?: IA[K]; // Equivalent to A3
};
```

## 6: Types versus interfaces

```ts
// Define function types
type F = (x: string) => string;
interface {
  (x:string): string; // Equivalent to F
}

// Define lists
type L = string[];
interface IF {
  [i: number]: string;
}

// Intersection and union types
interface IA {
  x: string;
}
interface IB {
  y: string;
}
// Define intersection types
type C = IA & IB;
interface IC extends IA, IB {}
class CC implements IA & IB {}
// Define union types
type C = IA | IB;
interface IC extends C {} // Error
class CC implements C {} // Error
// ^: An interface or a class can only extend intersections

```

## 8: self-referencing types

```ts
// Node type
interface LLNode<T> {
  value: T;
  next: LLNode<T> | undefined;
}

// Tip on traversing to the last node
do {
  curr = curr.next;
} while (curr);
```

## 9: iterator protocol

```ts
class MyIterator implements IterableIterator<string> {
  constructor(private _currNode: LLNode<string> | undefined) {}
  [Symbol.iterator](): IterableIterator<string> {
    return this;
  }
  next(): IteratorResult<string> {
    const curr = this._currNode;
    if (!curr) {
      return { value: undefined, done: true };
    }
    this._currNode = curr.next;
    return { value: curr.value, done: false };
  }
}
```

## 10: using unknown type to catch runtime errors

```ts
type F = () => any;
type G = () => unknown;

interface IA {
  x: sting;
}

let f: F;
let g: G;

f().a.b.c; // no error
g().a; // error: ‘a’ does not exist on type ‘unknown’

let r = g();

// Type guard for primitive types
if (typeof r === ‘string’) {
  t.toUpperCase();
// Type guard for object types
} else if (isIA(r)) {
  r.x;
// Cast if we’re certain the runtime type of r
} else {
  (<string[]>r).pop();
}

function isIA(a: any): a is IA {
  return (<IA>a).x !== undefined;
}
```

## 11: conditional types

```ts
interface IA {
  x: string;
}
interface IB {
  y: number;
}
type C<T> = {
  z: T extends string ? IA : IB;
}
const c1: C<string> = {
  z: {
    x: ‘’
  }
}
const c2: C<number> = {
  z: {
    y: 0
  }
}

type ArrayFilter<T> = T extends any[] ? T : never;
type Strings: ArrayFilter<string | string[]>; // Strings only has type string[]

type F<T extends string | number> = (x: T) => T extends string ? IA : IB;
let f1: F<string>;
f1(‘’).x;
let f2: F<number>;
f2(0).y;
```

## 12: Conditional types 2

```ts
type FlattenArray<T extends any[]> = T[number];
// ^: Returns the type of array elements, i.e. array index has type number

type FlattenObject<T extends object> = T[keyof T];

const a = [0];
type NumbersArrayFlattened = FlattenArray<typeof a>; // Has type number

const o = {
  a: 0,
  b: ""
};
type ObjectFlattened = FlattenObject<typeof o>; // Has type string | number

type Flatten<T> = T extends any[]
  ? T[number]
  : T extends object
  ? T[keyof T]
  : T;

type NumbersArrayFlattened2 = Flatten<typeof a>; // Has type number
type ObjectFlattened2 = Flatten<typeof o>; // Has type string | number

let p = "";
type PrimitiveValueFlattened = Flatten<typeof p>; // Has type string
const p2 = "";
type PrimitiveValueFlattened2 = Flatten<typeof p2>; // Has type ''
```

## 13: conditional type 3

```ts
const f = (x: number) =>
  Math.floor(Math.random() * 2) // Has return type string | number
    ? x + 1
    : x + "1";

type Return<T> = T extends (...args: infer K) => infer R ? R : any;
type t = Return<typeof f>; // Has type string | number

// TS has a build in type ReturnType
type t2 = ReturnType<typeof f>; // Has type string | number

type UnpackPromise<T> = T extends Promise<infer R> ? R : any;

const p = Promise.resolve(true);
type ExpectedBoolean = UnpackPromise<typeof p>; // Has type boolean
```

## 14: conditional type 4

```ts
interface IState {
  a: string[];
  b: {
    c: string;
  };
}

type DeepReadonlyObject<T> = { readonly [K in keyof T]: DeepReadonly<T[K]> };
type DeepReadonly<T> = T extends (infer E)[]
  ? ReadonlyArray<DeepReadonlyObject<E>>
  : T extends object
  ? DeepReadonlyObject<T>
  : T;

type ReadonlyIState = DeepReadonly<IState>;

// Reducer should return immutable state
const reducer = (action: any, state: ReadonlyIState): ReadonlyIState => {
  // Some action checks
  return state;
};

const state = reducer("whvr", {
  a: [""],
  b: {
    c: ""
  }
});

// state.b.c = '';
// ^: Error. c is readonly.
```

## 15: decorators

```ts
// Turn experimentalDecorators on in tsconfig.json
interface IData {
  a: string;
}

const callApi: Promise<IData> = Promise.resolve({ a: "" });

function GetData(target: any, name: string) {
  Object.defineProperty(target, name, {
    get: async () => await callApi
  });
}

class Service {
  @GetData
  data!: IData;
}

const s = new Service();
(async () => {
  console.assert((await s.data).a === "");
})();
```
